{-# LANGUAGE OverloadedStrings  #-}
module Lib where

import Data.Bool as BL
import Data.Monoid
import qualified Data.List as L
import qualified Data.Text as T
import qualified Data.Text.Read as T
import qualified Data.Maybe as M
import qualified Data.HashMap.Lazy as H

data Pattern = Pattern
             { groupPrefix :: T.Text
             , lengthCat   :: Int
             } deriving (Show)

findFirstOp :: [[ T.Text ]] -> Pattern
findFirstOp (x:xs)
  | T.isPrefixOf "operations" (colName x) = Pattern (getOpGroupPrefix (colName x)) (opNumCat (colName x))
  | otherwise = findFirstOp xs
  where colName x = x !! 9

findFirstBalance :: [[ T.Text ]] -> T.Text
findFirstBalance (x:xs)
  | T.isInfixOf "balance_updates" (colName x) = getBalanceGroupPrefix $ colName x
  | otherwise = findFirstBalance xs
  where colName x = x !! 9

getBalanceGroupPrefix :: T.Text -> T.Text
getBalanceGroupPrefix = newPat
   where newPat x = Prelude.head (token x) <> "balance_updates/" <> T.splitOn "/" (token x !!1) !! 1
         token = T.splitOn "balance_updates"

getOpGroupPrefix :: T.Text -> T.Text
getOpGroupPrefix t = T.drop 1 $ getOpGroupPrefix' (opNumCat t) "" tokens <> "/"
  where tokens = T.splitOn "/" t
        getOpGroupPrefix' :: Int -> T.Text -> [ T.Text ] -> T.Text
        getOpGroupPrefix' 0 r _ = r
        getOpGroupPrefix' i r (x:xs) = getOpGroupPrefix' (BL.bool i (i-1) $ isDigital x) (r <> "/" <> x) xs

opNumCat :: T.Text -> Int
opNumCat t = length $ filter isDigital $ skipProposals $ skipFitness $ skipOC $ skipSlots tokens
  where skipSlots x     = BL.bool x (init x) $ "slots" == target
        skipOC x        = BL.bool x (init x) $ "originated_contracts" == target
        skipFitness x   = BL.bool x (init x) $ "fitness" == target
        skipProposals x = BL.bool x (init x) $ "proposals" == target
        tokens = T.splitOn "/" t
        target = tokens  !! (length tokens - 2)

numCat :: T.Text -> Int
numCat t = length $ filter isDigital $ T.splitOn "/" t

isDigital :: T.Text -> Bool
isDigital t = case r of
                Left _ -> False
                Right (d, a) -> a == ""
    where r = T.decimal t

removeRule :: [ T.Text ] -> Bool
removeRule xs = not $ or r
    where t = xs !! 9
          r = [ T.isPrefixOf "operations/" t && T.isSuffixOf "/operation_result/parent_uuid" t
              , t == "header/content/command"
              , t == "header/parent_uuid"
              , t == "header/uuid"
              , t == "metadata/deactivated/0"
              , T.isPrefixOf "metadata/max_operation_list_length/" t
              , T.isPrefixOf "header/content/" t
              , t == "metadata/test_chain_status/uuid"
              , t == "metadata/level/parent_uuid"
              , t == "metadata/level/uuid"
              , t == "parent_uuid"
              , T.isSuffixOf "/metadata/parent_uuid" t && T.isPrefixOf "operations" t
              , T.isInfixOf "/internal_operation_results/" t && T.isSuffixOf "/result/parent_uuid" t
              , T.isSuffixOf "/op1/operations/parent_uuid" t
              , T.isSuffixOf "/op1/operations/uuid" t
              , T.isSuffixOf "/op2/operations/parent_uuid" t
              , T.isSuffixOf "/op2/operations/uuid" t
              ]

noIndex :: [ T.Text ] -> [ T.Text ]
noIndex xs = replaceNth 1 (T.pack $ show $ not $ isIndex t rules) xs
  where t = xs !! 9
        isIndex :: T.Text -> [ T.Text -> Bool ] -> Bool
        isIndex l [] = False
        isIndex l (f:xs)
          | f l = True
          | otherwise = isIndex l xs
        rules :: [ T.Text -> Bool ]
        rules =
          [ (isContainOf "operations/" "/operation_result/" "/storage")
          , (isContainOf "operations/" "/operation_result/" "/errors")
          , (isContainOf "operations/" "/operation_result/" "/parameters")
          , (isContainOf "operations/" "/operation_result/" "/big_map_diff")
          , (isContainOf "operations/" "/internal_operation_results/" "/storage")
          , (isContainOf "operations/" "/internal_operation_results/" "/errors")
          , (isContainOf "operations/" "/internal_operation_results/" "/parameters")
          , (isContainOf "operations/" "/internal_operation_results/" "/big_map_diff")
          , (isContainOf "operations/" "/contents/" "/parameters")
          , (isContainOf "operations/" "/contents/" "/script")
          ]

isContainOf
  :: T.Text -- ^ prefix
  -> T.Text -- ^ infix
  -> T.Text -- ^ suffix
  -> T.Text
  -> Bool
isContainOf p i s t
  =  T.isPrefixOf p t
  && T.isInfixOf i t
  && T.isSuffixOf s t

processOpCol :: T.Text -> [ T.Text ] -> [ T.Text ]
processOpCol l xs = replaceNth 8 c xs
   where c = M.fromMaybe t $ T.stripPrefix l t
         t = xs!!9

renameCol :: [ T.Text ] -> [ T.Text ]
renameCol xs = replaceNth 8 (newName t r) xs
  where t = xs !! 9
        newName :: T.Text -> [ (T.Text -> Bool, T.Text -> T.Text) ] -> T.Text
        newName l [] = l
        newName l ((c, n):ys)
           | c l = n l
           | otherwise = newName l ys
        r = [ (checkLastTwo "fitness"             , const "fitness")
            , (T.isPrefixOf "header/"             , T.drop (T.length "header/"))
            , (checkLastTwo "slots"               , const "slots")

            , (\x -> checkLastTwo "originated_contracts" x
                && T.isPrefixOf "operations/" x
                && T.isInfixOf "/operation_result/" x
                                                  , const "operation_result_originated_contracts")

            , (checkLastTwo "originated_contracts", const "originated_contracts")
            , (checkLastTwo "proposals"           , const "proposals")
            , (isLevelToCycle                     , const "cycle" )
            , (T.isSuffixOf "/managerPubkey", const "manager_pubkey" )
            , (T.isInfixOf "balance_updates"      , last . T.splitOn "/" )
            , (T.isPrefixOf "metadata/level/"     , T.drop (T.length "metadata/level/"))
            , (T.isPrefixOf "metadata/test_chain_status/", ("test_chain_" <>) . last . T.splitOn "/")
            , ( (== "metadata/parent_uuid")              , const "block_uuid")
            , (T.isPrefixOf "metadata/"                  , T.drop (T.length "metadata/"))
            , (isOpUuid                                  , const "op_uuid")

            , (T.isSuffixOf "/bh1/parent_uuid"           , const "double_baking_evidences_uuid")
            , (T.isSuffixOf "/bh2/parent_uuid"           , const "double_baking_evidences_uuid")
            , (T.isSuffixOf "/op1/parent_uuid"           , const "double_endorsement_evidences_uuid")
            , (T.isSuffixOf "/op2/parent_uuid"           , const "double_endorsement_evidences_uuid")
            , (T.isSuffixOf "/op1/operations/level"           , const "level")
            , (T.isSuffixOf "/op2/operations/level"           , const "level")
            , (T.isSuffixOf "/op1/operations/kind"           , const "kind")
            , (T.isSuffixOf "/op2/operations/kind"           , const "kind")
            , (\x -> T.isPrefixOf "operations/" x && T.isSuffixOf "/parent_uuid" x && numCat x == 2
                                                         , const "block_uuid")

            , (\x -> T.isPrefixOf "operations/" x && T.isInfixOf "/operation_result/" x
                                                         , ("operation_result_" <>) . last . T.splitOn "/")

            , (\x -> T.isPrefixOf "operations/" x
                && not (T.isInfixOf "internal_operation_results" x)
                && T.isInfixOf "/metadata/uuid" x
                                                         , const "metadata_uuid")

            , (\x -> T.isPrefixOf "operations/" x
                && not (T.isInfixOf "internal_operation_results" x)
                && T.isInfixOf "/metadata/" x
                                                         , last . T.splitOn "/")

            , (\x -> T.isPrefixOf "operations/" x && T.isSuffixOf "/hash" x
                                                         , const "op_hash")

            , (\x -> T.isPrefixOf "operations/" x && (T.isInfixOf "bh1" x || T.isInfixOf "bh2" x)
                                                         , \x -> T.drop (4 + T.length (getOpGroupPrefix x)) x)
            , (\x -> T.isPrefixOf "operations/" x && (T.isInfixOf "op1" x || T.isInfixOf "op2" x)
                                                         , \x -> T.drop (4 + T.length (getOpGroupPrefix x)) x)
            , (\x -> T.isPrefixOf "operations/" x 
                && T.isInfixOf "/internal_operation_results/" x
                && T.isInfixOf"/result/" x
                                                         , getAfterLastCat)
            , (\x -> T.isPrefixOf "operations/" x
                && T.isInfixOf "/internal_operation_results/" x
                && not (T.isInfixOf "/result/" x)
                && T.isSuffixOf "/parent_uuid" x
                                                         , const "op_metadata_uuid")

            , (T.isPrefixOf "operations"                 , \x -> T.drop (T.length (getOpGroupPrefix x)) x)

            ]

isOpUuid :: T.Text -> Bool
isOpUuid t = T.isPrefixOf "operations" t
           && numCat t == 3
           && isDigital (tokens !! 4)
           && (tokens !! 5) == "parent_uuid"
    where tokens = T.splitOn "/" t

isLevelToCycle :: T.Text -> Bool
isLevelToCycle t = T.isInfixOf "balance_updates" t
                 && last (T.splitOn "/" t) == "level"

checkLastTwo :: T.Text -> T.Text -> Bool
checkLastTwo k t = last tokens == "0" && target == k
  where tokens = T.splitOn "/" t
        target = tokens  !! (length tokens - 2)

getAfterLastCat :: T.Text -> T.Text
getAfterLastCat t = T.intercalate "_" $ L.drop (1 + findLastCat allCatIndex) tokens
  where tokens = T.splitOn "/" t
        allCatIndex = L.elemIndices True $ map isDigital tokens
        findLastCat [] = -1
        findLastCat x = last x

-- kind :
-- endorsement - level
-- seed_nonce_revelation - level, nonce
-- double_endorsement_evidence - op1, op2
-- double_baking_evidence - bh1, bh2
-- activate_account - pkh, secret
-- proposals - source, period, proposals
-- ballot - source, period, proposal, ballot
-- *reveal - *source, fee, counter, gas_limit, storage_limit, *public_key
-- *transaction
--   - *source, fee, counter, gas_limit, storage_limit, *amount, *destination
--   - *parameter?
-- *origination
--   - *source, fee, counter, gas_limit, storage_limit, *manager_pubkey(managerPubkey), *balance
--   - *spendable?, *delegatable?, *delegate?, *script?
-- delegation
--   - *source, fee, counter, gas_limit, storage_limit
--   - *delegate?


kindToTable :: [ ([ T.Text ], T.Text) ]
kindToTable = [([ "op1", "op2" ], "op_double_endorsement_evidences")
              , ([ "bh1", "bh2" ], "op_double_baking_evidences")
              , ([ "pkh", "secret" ], "op_activate_accounts")
              , ([ "source", "period", "proposal", "ballot" ], "op_ballots")
              , ([ "source", "period", "proposals" ], "op_proposals")
              , ([ "source", "fee", "counter", "gas_limit", "storage_limit", "public_key" ], "op_reveals")
              , ([ "source", "fee", "counter", "gas_limit", "storage_limit", "amount", "destination" ], "op_txs")
              , ([ "source", "fee", "counter", "gas_limit", "storage_limit", "manager_pubkey", "balance" ], "op_originations")
              , ([ "source", "fee", "counter", "gas_limit", "storage_limit", "managerPubkey", "balance" ], "op_originations")
              , ([ "source", "fee", "counter", "gas_limit", "storage_limit" ], "op_delegations")
              , ([ "level", "nonce" ], "op_seed_nonce_revelations")
              , ([ "level" ], "op_endorsements")
              ]

internalKindToTable :: [([ T.Text ], T.Text)]
internalKindToTable = [ ([ "nonce", "public_key" ], "internal_op_reveals")
                      , ([ "nonce", "amount", "destination" ], "internal_op_txs")
                      , ([ "nonce", "script", "balance" ], "internal_op_originations")
                      , ([ "nonce"], "internal_op_delegations")
                      ]
gpICntItem
  :: [[ T.Text ]]
  -> H.HashMap T.Text [ T.Text ]
  -> H.HashMap T.Text [ T.Text ]
gpICntItem [] h = h
gpICntItem (d:dt) h
  | T.isPrefixOf "operations" (path d)
     && opNumCat (path d) > 2
     && not (T.isInfixOf "balance_updates" $ path d)
     && T.isInfixOf "internal_operation_results" (path d)
     = gpICntItem dt $ H.insertWith (<>) (groupName d) ([col d] <> addCol additionRule) h

  | otherwise = gpICntItem dt h
  where col x = x !! 8
        path x = x !! 9
        addCol :: [ (T.Text, T.Text) ] -> [ T.Text ]
        addCol [] = []
        addCol ((p, t):xs)
          | T.isInfixOf p (path d) = [ t ]
          | otherwise = addCol xs
        additionRule :: [ (T.Text, T.Text) ]
        additionRule =
          [ ("/bh2/", "bh2")
          , ("/bh1/", "bh1")
          , ("/op1/", "op1")
          , ("/op2/", "op2")
          ]
        groupName x = x !! 2


gpCntItem
  :: [[ T.Text ]]
  -> H.HashMap T.Text [ T.Text ]
  -> H.HashMap T.Text [ T.Text ]
gpCntItem [] h = h
gpCntItem (d:dt) h
  | T.isPrefixOf "operations" (path d)
     && opNumCat (path d) > 2
     && not (T.isInfixOf "balance_updates" $ path d)
     && not (T.isInfixOf "internal_operation_results" $ path d)
     = gpCntItem dt $ H.insertWith (<>) (groupName d) ([col d] <> addCol additionRule) h

  | otherwise = gpCntItem dt h
  where col x = x !! 8
        path x = x !! 9
        addCol :: [ (T.Text, T.Text) ] -> [ T.Text ]
        addCol [] = []
        addCol ((p, t):xs)
          | T.isInfixOf p (path d) = [ t ]
          | otherwise = addCol xs
        additionRule :: [ (T.Text, T.Text) ]
        additionRule =
          [ ("/bh2/", "bh2")
          , ("/bh1/", "bh1")
          , ("/op1/", "op1")
          , ("/op2/", "op2")
          ]
        groupName x = x !! 2

renameOpTable :: H.HashMap T.Text T.Text -> [ T.Text ] -> [ T.Text ]
renameOpTable h ts
  | T.isPrefixOf "operations" path 
     && opNumCat path > 2
     && not (T.isInfixOf "balance_updates" path)
     && not (T.isInfixOf "internal_operation_results" path)
     && not (T.isInfixOf "/bh2/" path)
     && not (T.isInfixOf "/bh1/" path)
     && not (T.isInfixOf "/op1/" path)
     && not (T.isInfixOf "/op2/" path)
    = replaceNth 7 newTable ts
  | otherwise = ts
  where table = ts !! 7
        group  = ts !! 2
        path = ts !! 9
        newTable = M.fromMaybe table $ H.lookup group h

renameIOpTable :: H.HashMap T.Text T.Text -> [ T.Text ] -> [ T.Text ]
renameIOpTable h ts
  | T.isPrefixOf "operations" path 
     && opNumCat path > 2
     && not (T.isInfixOf "balance_updates" path)
     && T.isInfixOf "internal_operation_results" path
     && not (T.isInfixOf "/bh2/" path)
     && not (T.isInfixOf "/bh1/" path)
     && not (T.isInfixOf "/op1/" path)
     && not (T.isInfixOf "/op2/" path)
    = replaceNth 7 newTable ts
  | otherwise = ts
  where table = ts !! 7
        group  = ts !! 2
        path = ts !! 9
        newTable = M.fromMaybe table $ H.lookup group h

findKindTableByGroup :: H.HashMap T.Text [ T.Text ] -> H.HashMap T.Text T.Text
findKindTableByGroup = H.map (`findKindTable` kindToTable)

findIKindTableByGroup :: H.HashMap T.Text [ T.Text ] -> H.HashMap T.Text T.Text
findIKindTableByGroup = H.map (`findKindTable` internalKindToTable)

findKindTable :: [ T.Text ] -> [([ T.Text ], T.Text)] -> T.Text
findKindTable t [] = "defalut"
findKindTable t ((f,n):xs)
  | all (`elem` t) f = n
  | otherwise = findKindTable t xs

{-
- except blocks and block_alphas
- -}
allTable :: [ T.Text ]
allTable
  = [ "balances"
    , "bh1s"
    , "bh2s"
    , "internal_op_txs"
    , "op1s"
    , "op2s"
    , "op_activate_accounts"
    , "op_ballots"
    , "op_delegations"
    , "op_double_baking_evidences"
    , "op_double_endorsement_evidences"
    , "op_endorsements"
    , "op_originations"
    , "op_proposals"
    , "op_reveals"
    , "op_seed_nonce_revelations"
    , "op_txs"
    , "ops"
    ]

ruleToTable :: H.HashMap
                 T.Text      -- ^ talbe <> fields
                 ( T.Text      -- ^ field name in target table
                 , [ T.Text ]  -- ^ target table
                 )
ruleToTable
  = H.fromList
      [ ("blocks" <> "hash", ("block_hash", allTable))
      , ("blocks" <> "level", ("block_level", allTable))
      , ("blocks" <> "timestamp", ("block_timestamp", allTable))
      , ("block_alphas" <> "cycle", ("block_alpha_cycle", allTable))
      ]

copyOpHashRule :: [[ T.Text ]] -> [[ T.Text ]]
copyOpHashRule ts = ts ++ (filter (not . L.null) $ map copyOpRule' ts)
  where path x = x !! 9
        field x = x !! 8
        table x = x !! 7
        pathLeaf x = last $ T.splitOn "/" (x !! 9)
        prefixPath :: T.Text -> T.Text
        prefixPath = T.intercalate "/" . take 3 . T.splitOn "/"
        opHashTable :: H.HashMap T.Text [ T.Text ]
        opHashTable = H.fromList
                  $ map (\x -> (T.dropEnd 5 $ path x, x))
                  $ filter
                     (\x -> T.isPrefixOf "operations/" (path x)
                         && length (T.splitOn "/" (path x)) == 4
                         && T.isSuffixOf "/hash" (path x)) ts
        copyOpRule' :: [ T.Text ] -> [ T.Text ]
        copyOpRule' x
          = case H.lookup (prefixPath (path x)) $ opHashTable of
              Nothing -> []
              Just v
                -> if field x == "uuid"
                     then replace 2 (x !! 2)
                       $  replace 7 (table x)
                       $  replace 8 ("op_hash") v
                     else
                       []

copyAccRule :: [[ T.Text ]] -> [[ T.Text ]]
copyAccRule ts = ts ++ (reGruop $ (filter (not . L.null) $ map copyOpRule' ts))
  where path x = x !! 9
        field x = x !! 8
        pathLeaf x = last $ T.splitOn "/" (x !! 9)
        copyOpRule' :: [ T.Text ] -> [ T.Text ]
        copyOpRule' x
          |    pathLeaf x == "delegate"
            || pathLeaf x == "destination"
            || pathLeaf x == "pkh"
            || pathLeaf x == "source"
            || pathLeaf x == "baker"
            =   replace 7 ("accounts")
              $ replace 10 ("account")
              $ replace 8 ("account") x
          | otherwise = []
        reGruop :: [[T.Text]] -> [[T.Text]]
        reGruop x = map (\(i,k) -> replace 2  ("[Group:" <> T.pack (show i) <> "]") k) $ zip [1..] x

copyOpRule :: [[ T.Text ]] -> [[ T.Text ]]
copyOpRule ts = ts ++ (filter (not . L.null) $ map copyOpRule' ts)
  where path x = x !! 9
        field x = x !! 8
        table x = x !! 7
        prefixPath :: T.Text -> T.Text
        prefixPath = T.intercalate "/" . take 5 . T.splitOn "/"
        kindTable :: H.HashMap T.Text [ T.Text ]
        kindTable = H.fromList
                  $ map (\x -> (T.dropEnd 5 $ path x, x))
                  $ filter
                     (\x -> T.isPrefixOf "operations/" (path x)
                         && not (T.isInfixOf "/balance_updates/" (path x))
                         && T.isSuffixOf "/kind" (path x)) ts
        copyOpRule' :: [ T.Text ] -> [ T.Text ]
        copyOpRule' x
          = case H.lookup (prefixPath (path x)) $ kindTable of
              Nothing -> []
              Just v
                -> if field x == "uuid" && table x == "balances"
                     then replace 2 (x !! 2)
                       $  replace 7 (table x)
                       $  replace 8 ("op_kind") v
                     else
                       []

copyRule :: [[ T.Text ]] -> [[ T.Text ]]
copyRule ts = ts ++ replaceToFullRule ts
  where tableField x = x !! 7 <> x !! 8
        table x = x !! 7
        group x = x !! 2
        replaceToFullRule :: [[T.Text]] -> [[T.Text]]
        replaceToFullRule [] = []
        replaceToFullRule (x:xs)
          = case H.lookup (tableField x) ruleToTable of
              Just (newf,ts) -> (concat $ map (\y -> groupReplace $ replace 8 newf $ replace 7 y x) ts) ++ replaceToFullRule xs
              Nothing -> replaceToFullRule xs
        groupReplace :: [ T.Text ] -> [[ T.Text ]]
        groupReplace x = map (\g -> replace 2 g x) $ tableGroup ts (table x) []
        tableGroup
          :: [[ T.Text ]] -- ^ content
          -> T.Text       -- ^ table
          -> [ T.Text ]   -- ^ group
          -> [ T.Text ]   -- ^ group
        tableGroup [] _ g = g
        tableGroup (x:xs) t g
          | table x == t && (group x `elem` g) = tableGroup xs t g
          | table x == t =  tableGroup xs t $ group x : g
          | otherwise = tableGroup xs t g

replace :: (Show a) => Int -> a -> [a] -> [a]
replace i e xs = case L.splitAt i xs of
   (before, _:after) -> before ++ e: after
   _ -> xs

renameTable :: [ T.Text ] -> [ T.Text ]
renameTable ts = replaceNth 7 (newName path table r) ts
  where table = ts !! 7
        path = ts !! 9
        newName :: T.Text -> T.Text -> [ (T.Text -> Bool, T.Text) ] -> T.Text
        newName l o [] = o
        newName l o ((c, n):ys)
           | c l = n
           | otherwise = newName l o ys
        r = [ (T.isPrefixOf "header/", "blocks")
            , (T.isInfixOf "balance_updates", "balances")
            , (T.isPrefixOf "metadata/", "block_alphas")
            , (\x -> T.isPrefixOf "operations" x && numCat x == 2, "ops")
            , (T.isInfixOf "/bh1/", "bh1s")
            , (T.isInfixOf "/bh2/", "bh2s")
            , (T.isInfixOf "/op1/", "op1s")
            , (T.isInfixOf "/op2/", "op2s")
            ]

changeModifyRule :: [ T.Text ] -> [ T.Text ]
changeModifyRule ts = replaceNth 4 (newName p rule r) ts
  where rule = ts !! 4
        p = ts !! 9
        newName :: T.Text -> T.Text -> [ (T.Text -> Bool, T.Text) ] -> T.Text
        newName l o [] = o
        newName l o ((c, n):ys)
           | c l = n
           | otherwise = newName l o ys
        r = [ (checkLastTwo "fitness", "List")
            , (checkLastTwo "originated_contracts", "List")
            , (checkLastTwo "slots", "List")
            , (checkLastTwo "proposals", "List")
            ]

changeType :: [ T.Text ] -> [ T.Text ]
changeType ts = replaceNth 5 (newName path rule r) ts
  where rule = ts !! 5
        path = ts !! 9
        newName :: T.Text -> T.Text -> [ (T.Text -> Bool, T.Text -> T.Text) ] -> T.Text
        newName l o [] = o
        newName l o ((c, n):ys)
           | c l = n o
           | otherwise = newName l o ys
        r = [ (checkLastTwo "fitness", toArray)
            , (checkLastTwo "originated_contracts", toArray)
            , (checkLastTwo "slots", toArray)
            , (checkLastTwo "proposals", toArray)
            ]
        toArray x = "VDArray:" <> x

mkGpOpL2
  :: Pattern
  -> Int
  -> [[ T.Text ]]
  -> [[ T.Text ]]
mkGpOpL2 p@(Pattern pat n) gp [] = []
mkGpOpL2 p@(Pattern pat n) gp (d:dt)
  | T.isPrefixOf pat (col d)
     && opNumCat (col d) == n
     && opNumCat (col d) == 2
     && not (T.isInfixOf "balance_updates" $ col d)
     = processGp d gp : mkGpOpL2 p gp dt

  | T.isPrefixOf "operations" (col d)
     && opNumCat (col d) == 2
     && not (T.isInfixOf "balance_updates" $ col d)
     = processGp d (gp+1) : mkGpOpL2 newPat (gp+1) dt

  | otherwise = d : mkGpOpL2 p gp dt
  where col x = x !! 9
        newPat = Pattern (getOpGroupPrefix $ col d) (opNumCat $ col d)

mkGpIOp
  :: Pattern
  -> Int
  -> [[ T.Text ]]
  -> [[ T.Text ]]
mkGpIOp p@(Pattern pat n) gp [] = []
mkGpIOp p@(Pattern pat n) gp (d:dt)
  | T.isPrefixOf pat (col d)
     && opNumCat (col d) == n
     && opNumCat (col d) > 2
     && not (T.isInfixOf "balance_updates" $ col d)
     && T.isInfixOf "internal_operation_results" (col d)
     = processGp d gp : mkGpIOp p gp dt

  | T.isPrefixOf "operations" (col d)
     && opNumCat (col d) > 2
     && not (T.isInfixOf "balance_updates" $ col d)
     && T.isInfixOf "internal_operation_results" (col d)
     = processGp d (gp+1) : mkGpIOp newPat (gp+1) dt

  | otherwise = d : mkGpIOp p gp dt
  where col x = x !! 9
        newPat = Pattern (getOpGroupPrefix $ col d) (opNumCat $ col d)

mkGpOp
  :: Pattern
  -> Int
  -> [[ T.Text ]]
  -> [[ T.Text ]]
mkGpOp p@(Pattern pat n) gp [] = []
mkGpOp p@(Pattern pat n) gp (d:dt)
  | T.isPrefixOf pat (col d)
     && opNumCat (col d) == n
     && opNumCat (col d) > 2
     && not (T.isInfixOf "balance_updates" $ col d)
     && not (T.isInfixOf "internal_operation_results" $ col d)
     = processGp d gp : mkGpOp p gp dt

  | T.isPrefixOf "operations" (col d)
     && opNumCat (col d) > 2
     && not (T.isInfixOf "balance_updates" $ col d)
     && not (T.isInfixOf "internal_operation_results" $ col d)
     = processGp d (gp+1) : mkGpOp newPat (gp+1) dt

  | otherwise = d : mkGpOp p gp dt
  where col x = x !! 9
        newPat = Pattern (getOpGroupPrefix $ col d) (opNumCat $ col d)

mkGpBalance
  :: T.Text
  -> Int
  -> [[ T.Text ]]
  -> [[ T.Text ]]
mkGpBalance p gp [] = []
mkGpBalance pat gp (d:dt)
  | T.isPrefixOf pat (col d)
     && T.isInfixOf "balance_updates" (col d)
     = processGp d gp : mkGpBalance pat gp dt

  | not (T.isPrefixOf pat (col d))
     && T.isInfixOf "balance_updates" (col d)
     = processGp d (gp+1) : mkGpBalance (getBalanceGroupPrefix $ col d) (gp+1) dt

  | otherwise = d : mkGpBalance pat gp dt
  where col x = x !! 9
        group x = x !! 2
processGp :: [ T.Text ] -> Int -> [ T.Text ]
processGp xs i
  = replaceNth 2 (n (xs!!2)) xs
  where n :: T.Text -> T.Text
        n w = Prelude.head (T.splitOn ":" w) <> ":" <> T.pack (show i) <> "]"

replaceNth :: Int -> a -> [a] -> [a]
replaceNth _ _ [] = []
replaceNth n newVal (x:xs)
  | n == 0 = newVal:xs
  | otherwise = x:replaceNth (n-1) newVal xs

{-# LANGUAGE OverloadedStrings  #-}
module Main where

import Lib
import Data.Bool as BL
import qualified Data.Text as T
import qualified Data.Text.Read as T
import qualified Data.HashMap.Lazy as H
import Data.Char as C
import Data.Monoid
import Debug.Trace

main :: IO ()
main = do
  let path = "../baking-soda-tezos/index_config/tezos_block/config.origin"
  ctx <- readFile path
  let header = Prelude.head $ Prelude.lines ctx
      content :: [ String ]
      content = Prelude.tail $ Prelude.lines ctx
      tokenContent :: [[ T.Text ]]
      tokenContent = Prelude.map (Prelude.map T.pack . Prelude.words) content
  let fstP = findFirstOp tokenContent
      fstB = findFirstBalance tokenContent
  let conf = mkGpBalance fstB 0
           $ mkGpOpL2 fstP 0
           $ mkGpIOp fstP 0
           $ mkGpOp fstP 0
           $ map (noIndex . changeType . changeModifyRule . renameTable . renameCol)
           $ filter removeRule tokenContent
  let kindGroupTable = findKindTableByGroup $ gpCntItem conf H.empty
  let iKindGroupTable = findIKindTableByGroup $ gpICntItem conf H.empty
  let conf' = copyAccRule $ copyOpRule $ copyRule $ map (renameIOpTable iKindGroupTable . renameOpTable kindGroupTable) conf

  putStrLn header
  mapM_ (putStrLn . T.unpack . T.unwords) conf'
  return ()
